from pelican import signals
from re import sub


scopes = {"missing-guide": "https://schema.org/Guide"}


def _format(item: str, *args, **kwargs) -> str:
    return item.format(*args, **kwargs)


def _debug(value):
    print(value)
    return value


def lang_neutral(item: str) -> str:
    if len(item) < 1:
        return item
    chunks = item.split("/")
    if len(chunks) == 1:
        return chunks[0]
    else:
        return f"{chunks[-1]}/"


def _list_attributes(items: list, attr: str) -> list:
    return [item.__getattribute__(attr) for item in items]


def _itemscope(tags: list) -> str:
    _tags, scope = _list_attributes(tags, "name"), None
    for key, _scope in scopes.items():
        if key in _tags:
            scope = _scope
    if scope is None:
        return ""
    else:
        return f'itemscope itemtype="{scope}"'


def _html_cleaner(text: str) -> str:
    # This is really basic and not meant for use outside of api intern generated content
    return sub("\<.+?\>", "", text)


def _join(items: list, seperator: str) -> str:
    return str.join(seperator, items)


def _items(items: list, index: int = None, attribute: str = "") -> list:
    if index is None and attribute != "":
        return [item.__getattribute__(attribute) for item in items]
    elif attribute != "":
        return [item[index].__getattribute__(attribute) for item in items]
    else:
        return [item[index] for item in items]


def add_filter(pelican):
    """Add age_in_days filter to Pelican."""
    pelican.env.filters.update({"dir": dir})
    pelican.env.filters.update({"neutral": lang_neutral})
    pelican.env.filters.update({"debug": _debug})
    pelican.env.filters.update({"format": _format})
    pelican.env.filters.update({"attributes": _list_attributes})
    pelican.env.filters.update({"itemscope": _itemscope})
    pelican.env.filters.update({"join": _join})
    pelican.env.filters.update({"items": _items})
    pelican.env.filters.update({"clean": _html_cleaner})


# Module entry point
def register():
    # Connect the run hook function to the content_written signal
    # signals.finalized.connect(finished)
    # signals.all_generators_finalzed.connect(finished)
    signals.generator_init.connect(add_filter)
    # Temporary disabled
