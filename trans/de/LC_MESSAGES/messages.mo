��          �                 �    )   �     �             	   )     3  
   <     G  L   U     �     �     �     �  t  �  ~  A  #   �     �     �     
  	     	     
   '     2  F   B     �     �     �     �    The seminar will give you a brief introduction in modern web
        devlopment with python and pallet's flask. We will also teach you the
        basics of the modern web and how different services and interfaces
        interact to deliver you the seamless interactive web as you know it
        today. At the end of the seminar you will be able to utilise flask for
        your own project while creating 'beautiful' webpages with jinja2.  Andreas-Tag Computer Science Seminar 2020 Computer Science Seminar Brand Landing Page Projects Read More See more Seminar 20 Translations: Website for the computer science seminar: 'Web
development with python' 2020 Welcome to our seminar! articles en en_US Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2020-10-11 12:14+0200
PO-Revision-Date: 2020-10-11 12:15+0200
Last-Translator: 
Language: de
Language-Team: de <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.8.0
 Diese Seminar gibt euch eine Einführung in moderne Webentwicklung mit Python und Pallet's Flask. Wir werden auch die Grundlagen des modernen Webs und die verschiedenen Dienste behandeln, welche zu der heutigen nahtlosen Erfahrung beitragen. Am Ende des Seminars wirst du in der Lage sein Flask und Python für ein eigenes Projekt einzusetzen und Webseiten mit Jinja2 zu erstellen.  Andreas-Tag Informatik Seminar 2020 Informatik Seminar Brand Landing Page Projekte Lese mehr Lese mehr Seminar 20 Übersetzungen: Website für das Informatik-Seminar: "Web
Entwicklung mit Python" 2020 Willkommen zu unserem Seminar! Artikel de de_DE 