/*

Borrowed from cobalt.rocks

under GPLv3.0 @ Cobalt <chaosthe0rie@pm.me>

*/

document.addEventListener('DOMContentLoaded', function () {
  var Footer = document.getElementById('footer-advanced'),
    ArticlesTab = document.getElementById('pills-articles-tab');

  Footer.classList.add('btn-group');
  Footer.setAttribute('role', 'group');

  if (ArticlesTab.innerHTML === 'articles') {
    Footer.innerHTML +=
      '<a href="{{ SITEURL|neutral }}/de/blog/overview/" class="btn btn-sm btn-outline-white elegant-color white-text mt-0 mb-0" hreflang="de"><i class="fas fa-language"></i> DE</a>';
  } else {
    Footer.innerHTML +=
      '<a href="{{ SITEURL|neutral }}/blog/overview/" class="btn btn-sm btn-outline-white elegant-color white-text mt-0 mb-0" hreflang="en"><i class="fas fa-language"></i> EN</a>';
  }

  var triggerTabList = [].slice.call(document.querySelectorAll('#pills-tab a'));
  triggerTabList.forEach(function (triggerEl) {
    var tabTrigger = new mdb.Tab(triggerEl);

    triggerEl.addEventListener('click', function (e) {
      e.preventDefault();
      tabTrigger.show();
    });
  });
});
