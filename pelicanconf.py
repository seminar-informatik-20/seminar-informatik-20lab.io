#!/usr/bin/env python3
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = "Cobalt"
SITENAME = "Seminar Informatik 2020"
SITEURL = ""

PATH = "content"
THEME = "theme"
USE_FOLDER_AS_CATEGORY = True
OUTPUT_PATH = "public/"

TIMEZONE = "Europe/Berlin"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = False
CATEGORY_FEED_ATOM = False
TRANSLATION_FEED_ATOM = False
AUTHOR_FEED_ATOM = False
AUTHOR_FEED_RSS = False

# Direct template pages
TEMPLATE_PAGES = {
    "overview.html": "articles/overview/index.html",
    "robots.txt": "robots.txt",
}

MENUITEMS = (
    ("Projects", "/pages/projects/"),
    ("Contact", "/pages/contact/"),
    ("Articles", "/articles/overview/"),
)

# article url settings
ARTICLE_PATHS = ["articles"]
ARTICLE_URL = "articles/{category}/{slug}/"
ARTICLE_SAVE_AS = "articles/{category}/{slug}/index.html"
ARTICLE_LANG_SAVE_AS = "articles/{category}/{slug}-{lang}/index.html"
ARTICLE_LANG_URL = "articles/{category}/{slug}-{lang}/"
DRAFT_URL = "drafts/{slug}/index.html"
DRAFT_SAVE_AS = "drafts/{slug}/index.html"
DEFAULT_METADATA = {"status": "published"}

# Category url settings
CATEGORY_URL = "category/{slug}/"
CATEGORY_SAVE_AS = "category/{slug}/index.html"

# Page url settings
PAGE_PATHS = ["pages"]
PAGE_URL = "pages/{slug}/"
PAGE_SAVE_AS = "pages/{slug}/index.html"
PAGE_LANG_URL = "pages/{lang}/{slug}/"
PAGE_LANG_SAVE_AS = "pages/{lang}/{slug}/index.html"

# Prefixes
PREFIXES = ["/pages/*", "/articles/*", "/category/*"]

# name, description, version, link, class=""
DEFAULT_METADATA = {"status": "published"}


# mapping: language_code -> settings_overrides_dict
DEFAULT_LANG = "en"
I18N_SUBSITES = {
    "de": {
        "MENUITEMS": (
            ("Projekte", "/de/pages/projects/"),
            ("Kontakt", "/de/pages/contact/"),
            ("Artikel", "/de/articles/overview/"),
        ),
    }
}

# Plugins
PLUGIN_PATHS = ["plugins"]
PLUGINS = ["uglify", "filters", "i18n_subsites", "jinja2content"]
UGLIFY_EXCLUDE = [".+fontawesome.*$"]

# i18n Plugin Settings
JINJA_ENVIRONMENT = {"extensions": ["jinja2.ext.i18n"]}
I18N_GETTEXT_DOMAIN = "messages"
I18N_GETTEXT_LOCALEDIR = "trans"
