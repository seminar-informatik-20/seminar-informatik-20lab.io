Title: Flask Quickstart Tutorial
Date: 2020-3-27 10:20
Modified: 2020-3-27 10:20
Category: Guides
Tags: python, flask
Slug: flask-quickstart
lang: en
Authors: Cobalt
Summary: Short introduction to flask

## Setup

Working [Python 3](https://realpython.com/installing-python/) and [Pip](https://realpython.com/what-is-pip/#getting-started-with-pip)

You need to install the `flask` package:

`python3 -m pip install flask`

> A virtual environment is recommended!

## Explanation

(You should be able to run `python3 app.py` after every step, assuming you called this file `app.py`)

Initiating of the flask instance (in a new python file).

```python
from flask import Flask

app = Flask(__name__)
```

Running builtin development (debug = True -> more debug information) server when executing script with `python3` directly

```python
from flask import Flask

app = Flask(__name__)

if __name__ == "__main__":
    app.run(debug=True)
```

Declaring first route with html return (adding import `flask.render_template_string`):

```python
from flask import Flask, render_template_string

app = Flask(__name__)

@app.route("/")
def index():
    return render_template_string(
        """
            <h1>
                Hello World!
            </h1>
        """
    )


if __name__ == "__main__":
    app.run(debug=True)
```

<div class="break"></div> <!-- Those are for pdf conversion -->

Now you may be able to view the `Hello World!` from the url mentioned by the app (default = `httpS://localhost:5000/`).

You can start explaining html at this point and show the the cheat sheets. They should be able to test their HTML directly in teh above declared string.

Example with included css:

```python
from flask import Flask, render_template_string

app = Flask(__name__)


@app.route("/")
def index():
    return render_template_string(
        """
        <html>
            <body style="background-color: black; color: white; text-align:center;">
                <h1 style=" border-bottom: 1px solid white;">
                    Hello World!
                </h1>
                <p>
                    This is some paragraphed text.
                </p>
                <p>
                    <img src="https://cobalt.rocks/theme/images/brand.svg" width="100px" height="100px" alt="cat" />
                </p>
            </body>
        </html>
        """
    )


if __name__ == "__main__":
    app.run(debug=True)
```

<div class="break"></div> <!-- Those are for pdf conversion -->

You should now see (in your browser) something similar to:

<div style="margin: 2rem; background-color: black; color: white; text-align:center;"><h1 style="color: white !important; text-align:center !important; border-bottom: 1px solid white;">Hello World!</h1><p>This is some paragraphed text.</p><p><img src="https://cobalt.rocks/theme/images/brand.svg" width="100px" height="100px" alt="cat" /></p></div>
