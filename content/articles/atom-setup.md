Title: Atom Pair Programming Setup
Date: 2020-3-27 10:20
Modified: 2020-3-27 10:20
Category: Guides
Tags: atom, setup, help
Slug: atom-setup
lang: en
Authors: Cobalt
Summary: Setup guide for atom

## Installation

You can get atom for your operating system from [atom.io](https://atom.io/).

## Plugins

WIP

[Atom-pair](https://atom.io/packages/atom-pair) will be used for P2P-pair-programming.
