Title: Flask Quickstart Tutorial
Date: 2020-3-27 10:20
Modified: 2020-3-27 10:20
Category: Guides
Tags: python, flask
Slug: flask-quickstart
lang: de
Authors: Cobalt
Summary: Kurze Einführung zu Flask

## Setup

Funktionierende [Python 3](https://realpython.com/installing-python/) und [Pip](https://realpython.com/what-is-pip/#getting-started-with-pip) Installation

Du benötigst die `flask` Bibliothek:

`python3 -m pip install flask`

## Erklärung

> Du musst zuerst [Atom](/articles/atom-setup) aufgesetzt haben

Erstelle eine neues Projekt und eine Datei mit dem Namen `app.py`. Füge folgenden Text in die Datei ein:

```python
from flask import Flask

app = Flask(__name__)
```

Hierbei wird zuerst die Flask Klasse aus der `flask` Bibliothek importiert, diese wird daraufhin initiierst uns steht dir nun als `app` Objekt zur Verfügung. Diese Objekt können wir später benutzen um Routen zu registrieren. Das `__name__` Argument gibt dem Objekt die Möglichkeit sich an die ausführende Umgebung und den ausführenden Kontext anzupassen.

Jetzt müssen wir noch den mitgelieferten Entwicklungsserver ausführen. Um mehr Informationen zu bekommen erlauben wir diesem auch im `debug` Modus zu laufen. Vorher überprüfen wir aber ob das script direkt mit `python3 app.py` ausgeführt wird:

```python
from flask import Flask

app = Flask(__name__)

if __name__ == "__main__":
    app.run(debug=True)
```

Jetzt registrieren wir unsere erste `route`. Diese legt fest unter welcher Adress wir die darunter genannte Funktion finden können. Hierbei steht `/` für den `root` der Website, diese spezielle Route wird auch oft index genannt. Also bei `/` wird die darunter definierte Funktion unter `https://<hostname>:<port>/` bzw. `https://<hostname>:<port>` verfügbar sein. Um dem Browser auch etwas anzeigbares zurückzugeben verwenden wir die `render_template_string` Funktion aus der `flask` Bibliothek (sie muss zusätzlich importiert werden). Über das verwendete HTML erfährst du später mehr.

```python
from flask import Flask, render_template_string

app = Flask(__name__)

@app.route("/")
def index():
    return render_template_string(
        """
            <h1>
                Hall Welt!
            </h1>
        """
    )


if __name__ == "__main__":
    app.run(debug=True)
```

<div class="break"></div> <!-- Those are for pdf conversion -->

Jetzt solltest du mit deinem Browser die von Flask ausgegebene Url aufrufen können (hier ein Beispiel der Ausgabe):

```text
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
```

> Die Url ist der Link nach `Running on <link>`.

Wenn du weiter mit HTML dich ausprobieren möchtest kannst du dies gerne mit dem in `render_template_string` nagegeben `String` tun.

Beispiel mit eingebetten CSS:

```python
from flask import Flask, render_template_string  # import methods from flask library

app = Flask(__name__)  # Declare app instance


@app.route("/")
def index():
    return render_template_string(
        """
        <html>
            <body style="background-color: black; color: white; text-align:center;">
                <h1 style=" border-bottom: 1px solid white;">
                    Hello World!
                </h1>
                <p>
                    Das ist ein neuer Absatz
                </p>
                <p>
                    <img src="https://cobalt.rocks/theme/images/brand.svg" width="100px" height="100px" alt="cat" />
                </p>
            </body>
        </html>
        """
    )  # return example html


if __name__ == "__main__":   # Determine if script is run directly
    app.run(debug=True)
```

<div class="break"></div> <!-- Those are for pdf conversion -->

Diese Beispiel wird ähnlich wie folgendes aussehen:

<div style="margin: 2rem; background-color: black; color: white; text-align:center;"><h1 style="color: white !important; text-align:center !important; border-bottom: 1px solid white;">Hello World!</h1><p>Das ist ein neuer Absatz</p><p><img src="https://cobalt.rocks/theme/images/brand.svg" width="100px" height="100px" alt="cat" /></p></div>
