Title: Atom Pair Programming Setup
Date: 2020-3-27 10:20
Modified: 2020-3-27 10:20
Category: Guides
Tags: atom, setup, help
Slug: atom-setup
lang: de
Authors: Cobalt
Summary: Setup guide für atom

## Installation

Du kannst Atom für dein Betriebsystem von [atom.io](https://atom.io/) herunterladen.

## Plugins

WIP

Es wird [atom-pair](https://atom.io/packages/atom-pair) für P2P Pair Programming verwendet.
