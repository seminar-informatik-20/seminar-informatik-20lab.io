Title: Kontakt
Date: 2020-3-27 10:20
Modified: 2020-3-27 10:20
Category: General
Tags: general, trans-footer, headless
Slug: contact
lang: de
Authors: Cobalt
Summary: Kontakt zum Seminar
Clean: True

{% from "macros.jinja" import render_btn -%}

<div id="intro" class="container p-3 text-center">
  <div class="px-2">
    <div class="text-dark">
      <h1 class="h1">Kontakt</h1>
      <hr>
      <h5 class="mb-3 h5-responsive">Kontaktiere uns bei Fragen gerne</h5>
      <div class="btn-group mx-2 my-1 p-0" aria-label="Contact Forms">
        {{ render_btn("Email", "mailto:chaosthe0rie@protonmail.com", class="btn btn-lg btn-dark btn-email", icon="fas fa-envelope pr-1", itemtype="relatedLink") }}
        {{ render_btn("Matrix", "https://matrix.to/#/@c0ba1t:matrix.org", class="btn btn-lg btn-dark btn-email", icon="fas fa-comments  pr-1", itemtype="relatedLink") }}
      </div>
    </div>
  </div>
</div>
