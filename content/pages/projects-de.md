Title: Projekte
Date: 2020-3-27 10:20
Modified: 2020-3-27 10:20
Category: General
Tags: general, trans-footer, headless
Slug: projects
Authors: Cobalt
lang: de
Summary: Projekte und Dokumente über das Seminar
Clean: True

{% from "macros.jinja" import render_project  %}

<div id="intro" class="container text-center">
  <div class="d-flex justify-content-center px-2 align-items-center">
    <div class="text-dark">
      <h1 class="h1">Projekte</h1>
      <hr>
      <div class="row">
        <p>
            <small>Nach dem Seminar werden die Projekte der Teilnehmer hier auch teilweise verfügbar sein</small>
        </p>
      </div>
      <div class="row row-cols-1 row-cols-md-2 g-4 justify-content-center" itemscope itemtype="https://schema.org/CollectionPage">
        {{ render_project("Präsentation", "Alle Präsenationen für das Seminar", "Libre Office Impress", "fas fa-file-alt", "gitlab", "https://gitlab.com/seminar-informatik-20/presentations") }}
        {{ render_project("Dokumente", "Alle Dokumente für das Seminar", "Various", "fas fa-file-alt", "gitlab", "https://gitlab.com/seminar-informatik-20/documents") }}
        {{ render_project("Flask Quickstart", "Flask boilerplate", "Python 3.8.2", "fab fa-python", "gitlab", "https://gitlab.com/seminar-informatik-20/flask-quickstart") }}
        {{ render_project("Simple projects", "Flask Demontrations Projekte", "Python 3.8.2", "fab fa-python", "gitlab", "https://gitlab.com/seminar-informatik-20/simple-projects") }}
      </div>
    </div>
  </div>
</div>

<div class="row mt-2 p-0 justify-content-center">
  <p class="text-center lead text-black">Mehr Projekte:</p>
</div>
<div class="d-flex justify-content-center">
  <div class="btn-group mx-2 p-0 " aria-label="More projects">
    <a href=" https://gitlab.com/seminar-informatik-20" class="text-white btn btn-dark btn-lg btn-elegant btn-git"><i class="fab fa-gitlab"></i>   
        Gitlab
    </a>
  </div>
</div>
